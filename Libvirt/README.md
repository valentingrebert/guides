# Ces notes ont pour but de présenter la technologie Libvirt API (kvm/qemu) .

Le couple KVM/Qemu est un outil de virtualisation extrêmement puissant il bénéficie de nombreux avantages et intègre nativement et parfaitement les environnements CentOS/RHEL/Fedora.


SOMMAIRE : 


## I - La présentation de la technologie.

## II - La mise en place simple d'une Machine Virtuelle.

### A - Installation d'un système.

### B - Optimisation et journalisation des machines.

## III - Éléments du réseau de libvirt.

### A - Préparer un réseau isolé pour tests.

### B - Organiser une distribution LAN des machines.

## IV - La gestion des Machines virtuelles

### A - [Outils de connexion](https://gitlab.com/valentingrebert/guides/-/blob/master/Libvirt/Connections.md)

### B - Outils de sauvegarde/snapshot.