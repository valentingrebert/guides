# Se connecter à un hyperviseur et ses VM via libvirt (virt-manager) , cockpit, ssh et TLS.

---


### Sommaire : 

#### 1. Présentation
#### 2. Cockpit
#### 3. SSH
#### 4. TLS

--- ---

--- ---


### 1. Présentation

Pour gérer nos VM sur un hyperviseur de type kvm/qemu derrière *libvirt* nous devons nécessairement sécuriser le transport entre notre client et notre hyperviseur.

Il existe bien entendu le *secure shell* afin d’accéder à nos VM.

Un système orienté serveur est généralement dépourvu d'interface graphique or pour certaines raisons parfois nous avons besoin de GUI sur certaines machines. 

Ainsi afin d’accéder à ces machines nous pouvons utiliser le puissant outil *virt-viewer* qui prend en charge les modules internes à qemu.

Il existe une pléthore de méthodes de transport, entre autres : 

* tls,unix,ssh,ext,tcp,libssh,libssh2

 * Remarque: Nous excluons *libssh* en raison des énormes problèmes de sécurité de la [CVE-2018-10933](https://www.cvedetails.com/cve/CVE-2018-10933/)


La solution *cockpit* (puissante interface web) sur le port :9090 qui permet d’accéder à notre hyperviseur et aux machines virtuelles. (avec le paquet *cockpit-machines* )

Il faut par contre ouvrir le port :9090 ce qui dans certains cas n'est pas une option. 

Les deux autres méthodes qui vont attirer notre attention sont : *ssh* et *tls*.



--- ---


### 2. Cockpit

Afin d'utiliser cockpit nous gardons à l'esprit que le port :9090 devra être accessible sur notre hyperviseur.

Nous installons les outils nécessaires :

```shell
dnf install libvirt cockpit cockpit-machines
```

Si ça n'est pas déjà fait nous pouvons ajouter un utilisateur au groupe *libvirt* et *cockpit-ws* afin qu'il puisse accéder à l'interface web de cokpit et gérer les VM.

```shell
usermod -a -G libvirt,cockpit-ws user
```

Si aucune regle de pare-feu n'existe et que nous pouvons nous le permettre 

```shell
firewall-cmd --zone=public --add-service=cockpit --permanent
firewall-cmd --reload
```

Lançons notre service *cockpit*: 

```shell
systemctl enable --now cockpit.socket
```

Désormais en accédant à l'interface web de cockpit sur l'hyperviseur nous pourrons gérer nos machines directement depuis le navigateur web de notre client.

`https://IPHYPERVISEUR:9090`


![cockpit__machines](/uploads/a79f0bf5ecec76c0755d002a3c332c92/cockpit__machines.png)




### 3. SSH

La structure de nos commandes de connexion a cette forme :

`driver[+transport]://[username@][hostname][:port]/[path][?extraparameters]`

Afin d'utiliser *virt-manager* avec *ssh* il faudra une commande de type :

```shell
virt-manager -c 'qemu+ssh://hypervisoruser@IPHYPERVISOR:SSHPORT/system?keyfile=/home/user/.ssh/id_HYPERVISORSSHKEY'`
```

Cette commande permet de se connecter graphiquement au *virt-manager* de notre hyperviseur grâce à *qemu* et au protocole *ssh* en utilisant notre paire de clés préalablement configurée.

Si nous gérons une multitude de machine il est intéressant de créer des aliases: 

Dans `$HOME/.config/libvirt/libvirt.conf` afin que le client de notre *host* puisse utiliser ces alias. Il devra appartenir au groupe *libvirt* autrement les alias iront dans `/etc/libvirt/libvirt.conf`

Exemple: 

```shell
uri_aliases = [
 "hyperviseur=qemu+ssh://hypervisoruser@IPHYPERVISOR:SSHPORT/system?keyfile=/home/user/.ssh/id_HYPERVISORSSHKEY'",
 "hyperviseur2=qemu+ssh://hypervisoruser@IPHYPERVISOR2:SSHPORT/system?keyfile=/home/user/.ssh/id_HYPERVISOR2SSHKEY'",

]
```

Ces commandes permettent donc à un client de se connecter au *virt-manager* installé sur un hyperviseur en tapant simplement :

```shell
hyperviseur
```




### 4. TLS



Ressources :

https://libvirt.org/remote.html

https://wiki.libvirt.org/page/TLSSetup

https://wiki.libvirt.org/page/VNCTLSSetup#TLS_should_be_set_up_on_the_servers_first

