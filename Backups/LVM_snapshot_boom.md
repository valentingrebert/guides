# Préparer une sauvegarde bootable du volume (LV) du système d'un serveur physique.


---     ---

#### Nous allons préparer une sauvegarde directement liée au pool lvm de stockage et intégrée au menu de démarrage *grub2* .


Cette technique est assez simple à mettre en place et permet plusieurs choses : 

* Elle utilise l'utilitaire boom destiné à prendre en charge les snapshots lvm (introduit depuis RHEL 7.5).
* Elle permet de gérer beaucoup plus facilement les snapshots lvm.
* Cela permet de pallier au manque d'outils de snapshot sur un serveur physique car hors d'un hyperviseur.
* Elle intègre une entrée de menu dans grub2 pour démarrer directement le snapshot ou l'avant-snapshot.
___     ___

#### Prérequis :

* Un système RHEL 7.5 (minimum)
* La racine du système présente sur un Logical Volume.
* Les outils : `boom-boot boom-boot-grub`

ATTENTION : Cette technique présente la sauvegarde d'une racine système "/" veillez à adapter ces notes si votre "/var" est séparé.
La non sauvegarde de "/var" ou d'autres répertoires systèmes peut rendre cette technique inopérante. Soyez attentifs.

--- ---

## SOMMAIRE : 

### I - Installation et configuration 

### II - Suppression ou restauration du snapshot

___     ___



### I - Installation et configuration:


##### 1 - On installe les outils nécessaires de boot manager :

```shell
dnf install -y boom-boot boom-boot-grub
```

##### 2 - On scanne nos group volumes avec `vgs` :


```perl
  VG     #PV #LV #SN Attr   VSize  VFree 
  fedora   1   3   1 wz--n- 48,80g 16,73g
```

##### 3 - On scanne nos logical volumes avec `lvs` :


```perl
  LV      VG     Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root    fedora owi-aos--- 15,00g                                                    
  swap    fedora -wi-ao----  2,07g              
```


##### 4 - On crée notre volume de snaphsot. 

* Nous disposons de l'espace nécessaire alors nous créons avec l'option `-s`  un logical volume qui fera office de snapshot.

* Nous lui adressons la même taille que notre volume à sauvegarder. 

* Nous précisons également un nom ou une date.

```shell
lvcreate -s fedora/root -n root_snapshot_DATE -L 15G
```

##### 5 - Nous allons créer un profil dans boom pour qu'il détecte notre OS. 
Ici nous mettons en nom : el8 'Entreprise Linux 8) mais dans les prochaines version de *boom* la détection sera automatique. 
Cet argument fonctionne parfaitement sur Fedora 30 Server ( non testé sur 31 ).

```shell
boom profile create --from-host --uname-pattern el8
```

```perl
Created profile with os_id 80b7051:
  OS ID: "80b705195982f1fa5703ae3e66906c59651b64de",
  Name: "Fedora", Short name: "fedora",
  Version: "30 (Server Edition)", Version ID: "30",
  UTS release pattern: "el8",
  Kernel pattern: "/vmlinuz-%{version}", Initramfs pattern: "/initramfs-%{version}.img",
  Root options (LVM2): "rd.lvm.lv=%{lvm_root_lv}",
  Root options (BTRFS): "rootflags=%{btrfs_subvolume}",
  Options: "root=%{root_device} ro %{root_opts}" 
```


##### 6 - Nous sauvegardons au préalable le fichier `/boot/grub2/grub.cfg`


```shell
cp /boot/grub2/grub.cfg /boot/grub2/grub.cfg.bak
```



##### 7 - On relève le numéro OsID de notre profil en reprenant la liste :


```shell
boom profile list
```

```perl
OsID    Name                     OsVersion          
80b7051 Fedora                   30 (Server Edition)
```

##### 8 - Nous créons le profil avec le snaphsot :

```shell
boom create --profile 80b7051 --title "System Snapshot - 03/10/2019" --rootlv fedora/root_snapshot_DATE
```

Si l'on rencontre une erreur de type :

```perl
WARNING - Boom grub2 script missing from '/etc/grub.d'                                                                                                                                             
WARNING - Boom configuration not found in grub.cfg                                                                                                                                                 
WARNING - Run 'grub2-mkconfig > /boot/grub2/grub.cfg' to enable  
```

On lance comme indiqué la commande :

```shell
grub2-mkconfig > /boot/grub2/grub.cfg
```

La commande `boom list` vous permettra de lister les entrées ajoutées par *boom*.

Techniquement au reboot *grub2* vous affichera le nom de votre entrée "System Snapshot - 03/10/2019" qui lancera le système snaphshoté sur ce LV.



### II - Suppression ou restauration du snapshot:


Lorsque vos tests seront terminés il plusieurs possibilités :


##### 1 - Vos tests sont concluants. La mise à jour ou les modifications se sont bien passées et vous n'avez plus besoin du snapshot ni de son entrée *grub2*:


* A - Vous pouvez bien entendu vous débarrasser de l'entrée *grub2* et du lv snapshot avec les commandes suivantes : 

```shell
boom entry delete 80b7051
Deleted 1 entry
```

Ensuite supprimez votre LV snapshot: 

```shell
lvremove fedora/root_snapshot_DATE
```

```perl
Do you really want to remove active logical volume fedora/root_snapshot_DATE? [y/n]: y
 Logical volume "root_snapshot_DATE" successfully removed
```


* B - Conservez le LV de snapshot mais supprimez l'entrée *grub2* pour pouvoir reproduire cette procédure dans le futur (recommandé).



##### 2 - Vos tests sont dramatiques. La modification a altéré votre système. Vous voulez revenir à l’état d'origine : 

Vous ne voulez pas booter à partir du snapshot mais rétablir la situation avant snapshot.

Vous devez restaurer le LV snapshot

Vous pourrez utiliser la technique du merge.

* Avant d'effectuer une manipulation aussi drastique sur un système il est judicieux de faire une sauvegarde !
* Effectuez une sauvegarde de votre LV snapshot sur un dispositif de stockage externe : (réseau, disque):

Exemple pour une sauvegarde par sftp :

```shell
dd if=/dev/fedora/root_snapshot_DATE conv=notrunc status=progress | lz4c -9 | ssh -i .ssh/idserver user@host dd of=/var/backup/labserver.img.xz
```

Exemple pour une sauvegarde sur disque : 

```shell
dd if=/dev/fedora/root_snapshot_DATE conv=notrunc status=progress | lz4c -9 | dd of=/mnt/usb/backup.img.xz
```

Une fois la sauvegarde effectuée Vous pourrez utiliser la technique du merge.

```shell
lvconvert --merge /dev/fedora/root_snapshot_DATE 
```

```perl
 Delaying merge since snapshot is open.
 Merging of snapshot fedora/root_snapshot_DATE will occur on next activation of fedora/root.
```

Au redémarrage de l'entrée de base tout devrait rentrer dans l'ordre.


--- ---


#### Ressources : 

https://www.redhat.com/en/blog/boom-booting-rhel-lvm-snapshots

https://www.golinuxcloud.com/boom-boot-linux-lvm-snapshot-rhel-8-linux/

--- ---