# Presentation de differentes technologies de sauvegarde.


1 - [Le disaster recovery et le migrateur de systemes *ReaR*.](https://gitlab.com/valentingrebert/guides/-/blob/master/Backups/ReaR_disaster.md)

2 - [Préparer une sauvegarde bootable du volume (LV) du système d'un serveur physique.](https://gitlab.com/valentingrebert/guides/-/blob/master/Backups/LVM_snapshot_boom.md)

3 - La sauvegarde incrementielle journalisée et testée.

3 - L'externalisation des sauvegardes.