![openssh](/uploads/28480d10c2e240c156016ce4a794c362/openssh.gif)
# Mise en place d'un serveur SSH sécurisé sur une distribution GNU/Linux (type CentOS/RHEL/Fedora).

---

Statut: en perpétuelle construction.

---


#### Conventions :

Soyez attentifs aux commandes lorsque vous êtes en *root* # .

On considère que vous savez vérifier l'état, démarrer, arrêter, journaliser et activer au démarrage un service ou unit pour *systemd*.

```shell
man systemctl
man journalctl
```

Referez-vous à l'article sur [les bases de systemctl et journalctl](https://gitlab.com/valentingrebert/guides/blob/master/Sytemctl/README.md) pour de plus amples détails. 


--- ---


## SOMMAIRE:


[1 - TLDR; Résumé simple de mise en place de clés:](#part1)

[2. Présentation de SSH.](#part2)

[3. Configuration et sécurisation du serveur ssh.](#part3)

--- ---


--- ---





<a name="part1">

## 1 - TLDR; Résumé simple de la mise en place de clés:

</a>

__1. *Le client* génère une paire de clés dans son répertoire /home/user/.ssh/ avec la commande:__

```shell
ssh-keygen -t ed25519 -f .ssh/id_monserveur
``` 

__2. On configure *le serveur* pour accepter les authentifications par mot de passe avec le fichier `/etc/ssh/sshd_config` .__

```shell

vim /etc/ssh/sshd_config

```

__3. On copie la clé publique `$HOME/.ssh/id_monserveur.pub` du *client* vers le serveur avec la commande :__

```shell
ssh-copy-id -i .ssh/id_monserveur.pub user@host
```

__4. On configure *le serveur* pour ne plus accepter les authentifications par mot de passe et seulement par paire de clés.__

*Remarque: N'oubliez pas qu'afin que notre service *sshd* prenne en compte nos modifications il faut le redémarrer.

__5. On se connecte au serveur avec la clé privée :__

```shell
ssh -i .ssh/id_monserveur user@host
```

__6. On applique des contre-mesures :__


-----


<a name="part2">

## 2. Présentation de SSH.

</a>


Le protocole ssh **(secure shell)** est un outil primordial afin d’administrer un serveur Gnu/Linux il est fourni par [ **Openssh**](http://www.openssh.com/) .



Il est l'un des premiers outils à mettre en place, avec un pare-feu, sur un système GNU/Linux.

Nous allons voir comment mettre en place, utiliser et sécuriser une structure client/serveur ssh sur une distribution GNU/Linux de type CentOS/Red Hat/Fedora. 

La plupart de ces options s'appliquent également à d'autres distributions.

Pour mettre tout le monde d'accord [RTFM](https://man.openbsd.org/man8/sshd.8) !

Le service *sshd* est fourni par la paquet : `openssh-server` . 

Il est installé par défaut sur RHEL 8/Fedora 31. 

Il écoute le port définit par  l'[IANA](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers)  :22 et obéit aux options spécifiées dans le fichier: `/etc/ssh/sshd_config`.

Le principe même de ssh est d'utiliser des signatures digitales afin de vérifier l'identité entre client et serveur. 

Cette vérification permet ensuite de mettre en place un transport chiffré des données entre ces deux entités. 
Voici les 4 étapes clés:


1.   Le handshake initial permet au client de vérifier avec quel serveur il communique.
   
2.   La couche de transport entre les deux entités est chiffrée via un cipher symétrique.

3.   Le client s’authentifie auprès du serveur.

4.   Le client interagit avec le serveur derrière le chiffrement en place.


Les ciphers utilisés en phase 2 peuvent êtres discriminés.

Les utilisateurs accédant aux machines doivent disposer des droits adéquats mais aussi et surtout y accéder de façon sécurisée afin de réduire au maximum la surface d'attaque.

([MiTM](https://en.wikipedia.org/wiki/Man-in-the-middle_attack)  , [bruteforce](https://en.wikipedia.org/wiki/Brute_force) , ... ) 



-----
-----

<a name="part3">

## 3. Configuration et sécurisation du serveur ssh.

### A. Fichier de configuration.

### B. Paire de clés.

### C. Contre-mesures.

### D - Troubleshooting tips

</a>


--- ---

--- ---



### A. Fichier de configuration.

La configuration du service se fait par défaut au sein du fichier `/etc/ssh/sshd_config` .

 *Remarque : Ne copiez/collez pas cette configuration! 
Certaines options sont à préciser et à dé-commenter. 
 Soyez attentifs !
 Par ailleurs les commentaires sont là pour vous préciser à quoi servent les options.
(Pensez à également à spécifier le port si vous l'avez changé) .

*Soit vous pouvez copier sur le serveur votre clé publique `id_Server1.pub` dans `$HOME/.ssh/authorized_keys` user autorisé à se connecter en ssh

*Soit vous permettez l'authentification par mot de passe le temps de pouvoir effectuer cette copie . 

 ### __ATTENTION__ : 
 
* Lorsque vous modifiez votre fichier de configuration et redémarrez votre service il se peut que votre connexion soit coupée (broken pipe) ou que vous décidiez de vous déconnecter afin de vous reconnecter. 
Ne vous déconnectez pas !
Préparez un second terminal pour tester la connectivité avec votre serveur. Si vous avez des problèmes de connexion réglez ces problèmes dans votre session encore active. 


*Édition du ficher **coté serveur**:

`/etc/ssh/sshd_config`

```perl

# Choix du chiffrement
# Nous utilisons les rsa 4096 bits et les ed25519.
# l'ECDSA etant fortement deconseille

HostKey /etc/ssh/ssh_host_ed25519_key
HostKey /etc/ssh/ssh_host_rsa_key

# Désactiver l'authentification par mot de passe de root.

PermitRootLogin no
ChallengeResponseAuthentication no

# Autoriser l'authentification par mot de passe pour la copie de la clé publique seulement.
# Nous passerons impérativement l'option à no après cette copie. On active aussi al copie de clé si on copie via ssh-copy-id.

PasswordAuthentication yes

PubkeyAuthentication yes

# On activera ces options une fois notre paire de clés en place.
# AuthenticationMethods liste les methodes d'authentification autorisées
# Ici on force la publickey et rien d'autre.

# AuthenticationMethods publickey




# Pour plus de securité il est judicieux de desactiver P.A.M cependant
# RHEL/Fedora ne permettent pas que nous n'utilisions pas P.A.M. D'autres distributions Debian le permettent.

UsePAM yes

# Autoriser uniquement un utilisateur ou plusieurs utilisateurs.

# AllowUsers sshman,sftpman

# Désactiver l’accès par un compte sans mot de passe

PermitEmptyPasswords no

# Changer le port par défaut afin d’éviter les scripts automatiques de bruteforce.
# Dans cette configuration nous laissons le port par défaut.

Port 22


# On peut spécifier une plage d'IP spécifiques autorisées à se connecter.

#ListenAddress 192.168.1.*


# Présenter une bannière avertissant les utilisateurs sur l'identité du serveur.

Banner /etc/issue

# Forcer un timeout de session pour les utilisateurs ex: 300 secondes.

ClientAliveInterval 300
ClientAliveCountMax 0

# Désactivation de RSH et la vérification par host

IgnoreRhosts yes
HostbasedAuthentication no

# Afin d'avoir un suivi précis des logs d'utilisateurs et leur empreinte de clés.

LogLevel VERBOSE

# Logguer correctement les accès de fichiers par sftp

Subsystem sftp  /usr/lib/ssh/sftp-server -f AUTHPRIV -l INFO


# Utiliser les sandbox du noyau quand cela est possible.
# Renseignez vous sur Seccomp .

# UsePrivilegeSeparation sandbox

# Paramétrer le chroot afin de limiter les utilisateurs sftp. 
# Match group sftponly
#         ChrootDirectory /home/%u
          X11Forwarding no
#         AllowTcpForwarding no
#         ForceCommand internal-sftp
```

Afin de tester la validité de votre configuration du service nous utilisons la commande `ssh -T` 

### B. Clés par paires.

L'authentification par clés est une nécessité.
Nous ne traiterons pas de l'authentification par mots de passe car elle présente des désavantages relatifs à la sécurité.

En terme de chiffrement nous allons choisir un algorithme dit _state-of-the-art_ *X25519* : [ed25519](https://en.wikipedia.org/wiki/EdDSA)  appartenant à la famille des _elliptic curves_. 

Soyons attentifs **Openssh** permet la prise en charge de cet algorithme depuis la version [6.5](https://www.openssh.com/releasenotes.html#6.5) (2014) 
si vous disposez d'une version antérieure (mauvais) procédez à une mise à jour et si cela est impossible  utilisez l'algorithme RSA avec une clé de 4096 bits minimum. 

Pour plus de détails n’hésitez par à vous renseigner sur le chiffrement Post Quantum [PQC](https://en.wikipedia.org/wiki/Post-quantum_cryptography).


Il est bon de veiller [à ce jour (Openssh 6.7+] à une taille minimum des Diffie-Hellman générés par la configuration de `moduli` . 

Ce fichier de configuration se trouve dans `/etc/ssh/moduli` . 

Afin de changer la valeur pour un minimum de 3072 bits nous utilisons la puissante commande `awk` .

```shell
awk '$5 >= 3071' /etc/ssh/moduli > /etc/ssh/moduli.safe
mv -f /etc/ssh/moduli.safe /etc/ssh/moduli
```

Par ailleurs afin d'être sûrs que les clés ne sont pas compromises il est judicieux de supprimer les clés existantes et d'en générer de nouvelles au sein du répertoire `/etc/ssh/` :

On produit cette étape **coté serveur** . 

```shell
rm -vf /etc/ssh/ssh_host_*

ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key 

ssh-keygen -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key

chgrp ssh_keys /etc/ssh/ssh_host_ed25519_key /etc/ssh/ssh_host_rsa_key

chmod g+r,o-rwx /etc/ssh/ssh_host_ed25519_key /etc/ssh/ssh_host_rsa_key 
```

Nou limiterons aussi les algorithms, cipher et les MAC algorithms de l'echange de clés en modifiant la politique crypto du serveur:

```shell
cp /etc/crypto-policies/back-ends/opensshserver.config /etc/crypto-policies/back-ends/opensshserver.config.orig

echo -e "CRYPTO_POLICY='-oCiphers=chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr -oMACs=hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,umac-128-etm@openssh.com -oGSSAPIKexAlgorithms=gss-curve25519-sha256- -oKexAlgorithms=curve25519-sha256,curve25519-sha256@libssh.org,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group-exchange-sha256 -oHostKeyAlgorithms=ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,rsa-sha2-256,rsa-sha2-512,ssh-rsa,ssh-rsa-cert-v01@openssh.com -oPubkeyAcceptedKeyTypes=ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,rsa-sha2-256,rsa-sha2-512,ssh-rsa,ssh-rsa-cert-v01@openssh.com'" > /etc/crypto-policies/back-ends/opensshserver.config
```


### **Coté client** il nous faut générer une paire de clés (privée/publique) et copier la clé publique sur le serveur. 

La clé privée quant à elle devra rester protégée sur le client.

En ce qui concerne la génération de phrases de passe pour vos paires de clés il est nécessaire d'appliquer une politique rigoureuse de sécurité. 

Renseignez-vous sur l'[entropie](https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases) des phrases de passe. 

 * Remarque : Si vous utilisez une phrase de passe sur la clé vous devriez utiliser un outil de type *ssh-agent* avec *ssh-add*. 

On genere la clé :


```shell
ssh-keygen -t ed25519 -f ~/.ssh/id_Server1
```

On la copie soit en se connectant avec mot de passe autorisé.

```
ssh-copy-id -i ~/.ssh/id_Server1.pub user@host
```


Soit en copiant/collant le contenu de la clé publique : `~/.ssh/id_Server1.pub` sur le `authorized_keys` du user du server .

Coté client :

```bash
cat ~/.ssh/id_Server1.pub
```


Coté serveur :

```bash 
vim /home/monuser/.ssh/authorized_keys
```

On colle le contenu.

On redemare le serveur ssh

```bash
systemctl restart sshd
```


Une fois la clé copiée testez la connexion **coté client**  dans un second terminal pour être assuré que cette méthode fonctionnera.



```shell
ssh -i ~/.ssh/id_Server1 user@host
```

Si la connectivité est OK vous devrez modifier à nouveau votre configuration de serveur en **modifiant/activant** ces options 

## Coté Serveur:

`/etc/ssh/sshd`

```perl
PasswordAuthentication no
AuthenticationMethods publickey
```

On peut tester notre configuration avec `sshd -t` avant de relancer notre serveur:



```shell
systemctl restart sshd
```

Coté client on test à nouveau la connexion cette fois-ci avec les clés:

```shell
ssh -v -i .ssh/id_macléprivée user@host
```


-----

### C - Contre-mesures.


__Le chroot:__

Il est préconisé de veiller à permettre la connexion à un utilisateur qui n'est pas membre du groupe `wheel` (`sudo` pour les distributions Debian).
 
En effet si le client a été compromis et sa paire de clés également l'attaquant pourra éventuellement, selon les méthodes utilisées et les politiques de mots de passe, accéder au serveur et escalader ses droits vers *root*. 

Une technique consiste à mettre en place un utilisateur avec peu de droits, confiné à un répertoire. Cet utilisateur devra ensuite passer en *root*. 

On peut aussi enfermer *jail* l'utilisateur dans un *chroot* .


__Le port:__

Nous pourrons changer le port :22 par défaut afin d’éviter les scans et tentatives de bruteforce de scripts automatisés mais cela n'augmente pas en soi la sécurité inhérente au service.

Naturellement nous devrons adapter notre politique de pare-feu à ce changement.

Nous devrons par ailleurs correctement informer les clients du changement de ce port.

Aussi il faudra veiller à renseigner ce changement dans d’éventuels outils de monitoring/supervision ou de détection d'intrusion (IDS NIDS).

Cela ne dispense pas de configurer un IDS ! (sshguard , fail2ban, Surricata... )


### D - Troubleshooting tips


* Lorsque vous rencontrez le message : ` no route to host` il s'agit d'un problème réseau . 

Vérifiez auparavant la connectivité avec un ping. 



* Lorsque vous obtenez l'erreur : `Permission denied publickey,gssapi-keyex,gssapi-with-mic)`

Il peut s'agir de différents problèmes. 

Vous pouvez utiliser le mode verbose avec `ssh -v` coté client et aussi journaliser votre service `sshd` sur le serveur avec `journalctl`.

Par exemple:

```shell
journalctl -f -b -u sshd
Authentication refused: bad ownership or modes for directory $HOME/.ssh
```
Il faudra ajuster les permissions correctes coté serveur avec :

```shell
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys
```

* Lorsque vous voyez ce message lors d'une tentative de connexion infructueuse:

```console
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ED25519 key sent by the remote host is
```

Cela signifie que quelque chose à changé par rapport à votre dernière connexion au serveur.
Cela peut être son IP, le nom de host ou d'autres éléments.

Certains sont d'avis d'ignorer ce message et de modifier le fichier `.ssh/known_hosts` or ce message peut signifier en effet qu'une attaquant s'est placé entre vous et le serveur. Investiguez si le doute est permis.

* Lorsque vous rencontrez cette erreur: 

```shell
Received disconnect from 123.123.132.132: Too many authentication failures for hostname
```

Il s'agit certainement de votre agent `ssh-agent` qui dispose de trop de clés pour le même serveur.
Il peut également s'agir de l'option `MaxAuthTries` qui bloque les tentatives au delà d'un certain nombre de connections.

3 solutions s'offrent à vous: 

1 - Réinitialisez votre ssh-agent en lui flushant les clés. Cette commande ne supprime aucune clé dans `$HOME/.ssh/` elle flush l'agent seulement.

```shell
ssh-add -D
```

2 - Precisez dans la commande de connection ou de copie de clés qu'il faut forcer l'auhtentification autorisée.
avec l'argument : `-o IdentitiesOnly=yes`

```shell
ssh -o IdentitiesOnly=yes -i ~/.ssh/macléprivée user@host
```

3 - Configurez votre client avec `$HOME/.ssh/config`

```console
Host host
 Hostname host
 User user
 port 22
 IdentitiesOnly yes
 IdentityFile ~/.ssh/qmachines
```

* CF Manuel :

```perl
IdentitiesOnly
Specifies that ssh(1) should only use the authentication identity files configured in the ssh_config files, even if ssh-agent(1) offers more identities. The argument to this keyword must be ''yes'' or ''no''. This option is intended for situations where ssh-agent offers many different identities. The default is ''no''.

IdentityFile
Specifies a file from which the user's RSA or DSA authentication identity is read. The default is ~/.ssh/identity for protocol version 1, and ~/.ssh/id_rsa and ~/.ssh/id_dsa for protocol version 2. Additionally, any identities represented by the authentication agent will be used for authentication.

The file name may use the tilde syntax to refer to a user's home directory or one of the following escape characters: '%d' (local user's home directory), '%u' (local user name), '%l' (local host name), '%h' (remote host name) or '%r' (remote user name).

It is possible to have multiple identity files specified in configuration files; all these identities will be tried in sequence. 
```


### Le chroot/jail :

[Cette méthode](https://access.redhat.com/discussions/3222641)
est complexe et s'applique dans un contexte spécifique de sécurité.

S'il existe une méthode plus simple l'auteur s'engage à éditer ces notes des qu'il en aura pris connaissance.




### 3. SFTP  Chroot/Jail .

</a>

On crée le groupe *sftpgroup* et l'utilisateur *sftpman* qui ne pourra pas se connecter en *ssh* mais seulement utiliser le sous-système *sftp*.

Pour cela nous pouvons l'exclure du shell grâce à `/sbin/nologin` et nous lui précisons son répertoire de travail `/srv` par exemple.

 * Coté Serveur :

```shell
groupadd sftpgroup
useradd -d /srv/ -s /sbin/nologin -M -N -g sftpgroup sftpman
passwd sftpman
```

`-d` spécifie le répertoire de travail

`-s` spécifie le login : Ici il ne pourra se loguer via ssh il pourra seulement utiliser le *sftp* (avec la commande scp ou rsync par exemple).

`-M` ne crée pas de home pour *sftpman*

`-N` ne crée pas de groupe portant le même nom que l'utilisateur

`-g` place l'utilisateur dans le groupe *sftpgroup*

Nous éditons la configuration de *sshd* et ajoutons le chroot/jail: 

`/etc/ssh/sshd_config`

```console
LogLevel VERBOSE
Subsystem sftp internal-sftp
```

Le jail se fait pour tout le groupe sftp et on jail dans /srv car les droits de la racine du *jail* doivent être `root:root 755` 

On peut également agir sur les droits de /srv/data avec `setfacl` et le groupe *sftpgroup*.

```console
Match Group sftpgroup
  X11Forwarding no
  AllowTcpForwarding no
  ChrootDirectory /srv
  ForceCommand internal-sftp -l VERBOSE
  PasswordAuthentication yes
```

Nous précisons que nous voulons du log `VERBOSE` pour le service *internal-sftp* car sans cela on ne parvient pas à bien savoir ce qui se passe sur le serveur en analysant le `/var/log/secure`.

Nous ajoutons `PasswordAuthentication yes` au sein de la jail de telle sorte à ce que sftpman puisse utiliser le mot de passe. En revanche *sshman* utilisateur classique se connectera toujours par paire de clés.

Cependant nous pourrions et devrions attribuer une paire de clés à l'utilisateur *sftpman* pour une plus grande sécurité.
Il suffit de reproduire les étapes de création/copie des clés sur le serveur.

Afin d’éviter de rencontrer les erreurs de type : 

`fatal: bad ownership or modes for chroot directory '/srv/data'`
`pam_unix(sshd:session): session closed for user sftpman`

Nous paramétrons les droits correctement car comme le dit le manuel de *sshd* :

>  All components of the pathname must be root-owned directories that are not writable by any other user or group. After the chroot, sshd(8) changes the working directory to the user's home directory.

```shell
chown -R root:root /srv 
chmod -R go-w /srv
```

Maintenant coté client nous testons le *sftp*. C'est ici que *tmux* peut être d'une grande aide en loguant dans une autre fenêtre.

Pour éviter également les erreur d’accès de type :

```shell
localhost sshd[3486]: sent status Permission denied
```

---



### Prévenir le bruteforce.

Afin de monitorer les tentatives de connexions effectuées par des attaquants et/ou script kiddies il existe différents outils.

On peut penser que ces outils sont inutiles en cas d'authentification par clés or il est nécessaire de conserver une trace des activités malveillantes à l'encontre de nos serveurs.

Il faut noter la différence entre:

* les NIDS (Network Intrusion Detection Systems)
* les HIDS (Host Intrusion Detection Systems )

Aussi afin de décourager ces pratiques d'attaques les outils de blocage tels *tcpwrapper*, *fail2ban*, *pare-feu*, *pam*, ... seront de rigueur.

*fail2ban* ou *sshguard* !TODO!
-----

### Le réseau

Au niveau du réseau d'autres pratiques sont de rigueur.

1 - Changez le port :22 des que cela vous est permis  en modifiant la valeur de `Port` dans le fichier de conf.

2 - L'utilisation et la configuration d'un pare-feu est juste primordiale. Pour les distribution RHEL CentOS Fedora nous utilisons **firewalld** qui se configure avec `firewall-cmd`.

Nous ne traiterons pas des détails du réseau mais seulement quelques bonnes pratiques et quelques commandes :

Exemple: (Notez qu'ici le port est le port par défaut )

```shelll
firewall-cmd --add-port=22/tcp
firewall-cmd --reload
```

Une fois nos tests effectués et concluants nous pouvons appliquer la règle de manière définitive. 

```shell
firewall-cmd --permanent --add-port=22/tcp
firewall-cmd --reload
```

Si vous l'avez changé adaptez la commande.

Il peut être intéressant de se pencher sur _tcpwrapper_ et le _portknocking_ mais cela fera l'objet d'un autre chapitre.


-----

Ressources : 

https://infosec.mozilla.org/guidelines/openssh

https://www.cyberciti.biz/tips/linux-unix-bsd-openssh-server-best-practices.html

https://man.openbsd.org/man8/sshd.8

https://docs.fedoraproject.org/en-US/fedora/f31/system-administrators-guide/infrastructure-services/OpenSSH/

https://www.ssh-audit.com/hardening_guides.html#rhel8

-----

-----

- Auteur: Valentin

- Contact: studentrhcsa@pm.me

- Site: Gitlab

- Date: 06/09/2019



