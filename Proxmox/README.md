# Installation d'un proxmox over debian 11 en EFI LUKS LVM btrfs.

**Avant Propos**

Installer Debian 11 en UEFI (plus de flexibilité dans l'intallation)
LUKS (chiffrement) sauf boot pour dechiffrer à distance
LVM + BTRFS pour faire des snapshots du proxmox lui même


* On propose dans l'appendice egalement de chiffrer le boot mais il sera imossible de  pouvoir debloquer le systeme au demarrage à distance via dropbear-ssh.

* Cette installation a un autre but: 
Pouvoir automatiser la technique avec Preseed + Ansible afin d'installer des serveurs tels que celui-ci automatiquement via un serveur PXE.

En ce qui concerne l'espace reservé aux qm/lxc on propose deux pistes : 

* X. Faire des snapshots lxc via le lvm thin provisionning.
Nous conserverons de l'espace sur le VG il servira à monter un thinprovisionning.

OU

* Y. Utiliser la puissance de btrfs sur tout le VG `nuxluks-pve--root` (systeme peu volumineux) et faire les backups des qm/lxc vers externe [PBS eventuellement] (en chiffrant sauvegarde/transport).


__Nous pourrions ajouter à cela une image de notre proxmox finalisé copiée avec clonezilla sur un NAS chiffré ou un PBS pour pouvoir redeployer le proxmox fresh sans avoir à tout recommmencer. (cette technique peut s'appliquer à plusieurs proxmox)__

##### Soit :

* Debian en 192.168.1.30

* Routeur en 192.168.1.1

* SSD de 250GB

* Un Filesystem en BTRFS

* Une Interface ethernet bridgée sur vmbr0


### 1 - [Installation de Debian 11](https://gitlab.com/valentingrebert/guides/-/blob/master/Install%20serveur%20chiffr%C3%A9/README.md)


### 2 - Installation de proxmox on top of debian 11

*ATTENTION
On fait en sorte que notre le fichier interfaces soit conforme : 
Nous ajoutons la ligne: `auto eno1` au dessus de iface de sorte à ce que le fichier ressemble à ça :

`/etc/network/interfaces`

```
# Please do NOT modify this file directly, unless you know what
# you're doing.
#
# If you want to manage parts of the network configuration manually,
# please utilize the 'source' or 'source-directory' directives to do
# so.
# PVE will preserve these directives, but will NOT read its network
# configuration from sourced files, so do not attempt to move any of
# the PVE managed interfaces into external files!

source /etc/network/interfaces.d/*

auto lo
iface lo inet loopback

auto eno1
iface eno1 inet dhcp

auto vmbr0
iface vmbr0 inet static
        address 192.168.1.30/24
        gateway 192.168.1.1
        bridge-ports eno1
        bridge-stp off
        bridge-fd 0

```


Maintenant nous pouvons installer proxmox comme la doc officielle le preconise :


On adapte ce fichier au format de proxmox :

`/etc/hosts`

```
127.0.0.1       localhost
192.168.1.30   zhurong.marslab zhurong

#127.0.1.1      zhurong.marslab zhurong

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```

On fait une full upgrade : 

```bash
apt update && apt full-upgrade -y
```

Si fat update avec kernel on reboot avant d'installer.

On ajoute les depots qui vont bien :

```bash
echo "deb [arch=amd64] http://download.proxmox.com/debian/pve bullseye pve-no-subscription" > /etc/apt/sources.list.d/pve-install-repo.list
```

On DL la clé gpg et on la verifie : 

```bash
wget https://enterprise.proxmox.com/debian/proxmox-release-bullseye.gpg -O /etc/apt/trusted.gpg.d/proxmox-release-bullseye.gpg 
sha512sum /etc/apt/trusted.gpg.d/proxmox-release-bullseye.gpg
7fb03ec8a1675723d2853b84aa4fdb49a46a3bb72b9951361488bfd19b29aab0a789a4f8c7406e71a69aabbc727c936d3549731c4659ffa1a08f44db8fdcebfa  /etc/apt/trusted.gpg.d/proxmox-release-bullseye.gpg 
```

On update la liste des paquets : 

```bash
apt update
```

On enleve les depots entreprise s'ils ne nous interessent pas :

```bash
rm etc/apt/sources.list.d/pve-enterprise.list 
```

Et c'est parti, enfin presque :

* L'installation se passe bien excepté si on a mal configuré son `/etc/network/interfaces` on peut tomber là dessus :
`dpkg: warning: while removing firmware-linux-free, directory '/lib/firmware' not empty so not removed
Removing ifupdown (0.8.36) ...`
Ceci va litteralement peter le DHCP et vous drevez relancer dhcclient sur votre serveur. Si vous avez un doute anticipez en envoyant avant la commande `sleep 2m dhcclient -r && dhclient &` ou whatsoever else.

Donc là c'est parti : 

```bash
apt install proxmox-ve postfix open-iscsi
```

Un TUI apparait nous demandant ce qu'on veut pour le mail. 
Nous mettons localonly il est possible de le configurer plus tard.

Ok cool on reboot.

On vire `os-prober` pour eviter des désagrements dans grub : 

```bash
apt remove os-prober
```

On vire les kernel de Debian : Ne vous inquietez pas nos confs de l'initramfs perdurent et fonctionnent :

```bash
apt remove linux-image-amd64 'linux-image-5.10*'
```

On update le grub

```bash
update-grub
```

### 3 - Post installation, premier snapshot et image du full-system.

Ici se posent deux choix : 

* X. Nous etendons le lvm pour faire un proxmox en FULL BTRFS sur le même logical volume
* Y. Nous créons un autre logical volume en thin provisionning pour nos QM/LXC (snapshots)


X. Nous étendons notre root Logical Volume (LV) pour avoir tout le proxmox en btrfs :

```bash
lvextend -l 100%FREE /dev/nuxluks/pve-root
btrfs filesystem resize max /
btrfs filesystem usage / 
```

Au final nous obtenons ceci : 

```
sda                       8:0    0 238.5G  0 disk  
├─sda1                    8:1    0   487M  0 part  /boot/efi
├─sda2                    8:2    0   954M  0 part  /boot
└─sda3                    8:3    0 237.1G  0 part  
  └─sda3_crypt          253:0    0 237.1G  0 crypt 
    ├─nuxluks-pve--root 253:1    0   183G  0 lvm   /
    └─nuxluks-pve--swap 253:2    0   7.4G  0 lvm   [SWAP]
```

Il reste un peu d'espace dispnible dans le VG
Nous allons ensuite installer et configurer des outils tres utiles avant de proceder à un imaging du disque hebergant le proxmox.

```bash
apt install -y apt-list-changes cron-apt git man-db netcat tmux vim wget
```

On configure cron-apt pour qu'il nous envoie des mails en fonction des mises à jour disponibles. 
Il faudra configurer le service postfix selon votre mail provider.



Nous allons utiliser un autre subvolume pour stocker nos snapshots 

```bash
btrfs subvolume create /snapshots
```

Go snapshot !






APPENDICES : 

Y. Nous créeons un thin provisionning sur l'espace restant. 

Il faudra lui donner un poolthinmetada de 2G faites moi confiance ça sert.
Auparavant assurez vous qu'il reste de la place dans votre VG avec la commande `vgs`

```bash
lvcreate -l 100%FREE --poolmetadatasize 2048M --chunksize 256 -T nuxluks/pvethin
```

Voila ça devrait le faire. (non testé pour cet article) si jamais le coeur vous en dit utilisez le GUI mais tout le monde sait que les GUI c'est caca.



RESSOURCES :



https://www.dwarmstrong.org/remote-unlock-dropbear/

https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html

https://pve.proxmox.com/wiki/Install_Proxmox_VE_on_Debian_11_Bullseye

https://pve.proxmox.com/wiki/Storage:_LVM_Thin

https://engineerworkshop.com/blog/lvm-thin-provisioning-and-monitoring-storage-use-a-case-study/

https://www.diytechguru.com/2020/12/12/create-lvm-thin-pool-in-proxmox/



