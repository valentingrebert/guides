*   [ ] Préparation VM 18.04 et jonction à l&#39;A.D

Préambule : Il s'agit d&#39;une installation manuelle. À terme nous fournirons des scripts preseed et une méthode Ansible afin d&#39;installer et de mettre au domaine automatiquement.

SOMMAIRE :

[1 - Installation de la VM et du système:]
[2 - Mise au domaine:]

## 1 - Installation de la VM et du système.

Rien de compliqué jusqu&#39;ici . On crée une VM avec les specs qu&#39;on estime nécessaires.

On définit un admin local

La VM Fraîchement installée on passe en root on met à jour et on installe les paquets qui vont bien.


```bash
# apt update && apt full-upgrade
```

On reboot. Pas nécessaire mais charger le dernier kernel et les derniers outils peut être une bonne chose avant de se lancer.

On installe les prérequis pour l'utilisation.

```bash
# apt update && apt install -y vim git tmux build-essential open-vm-tools sssd wget curl python libpam-sss libnss-sss krb5-user samba libnss-winbind realmd chrony htop glances ubuntu-restricted-extras gnome-shell-extensions vlc scribus inkscape gimp ocsinventory-agent grub2-splashimages               
```

Nous aurons durant l'installation une passage en TUI (Text User Interface) générés par dpkg.

1 - Pour renseigner Kerberos 5.

Il y a moyen d&'automatiser tou cela dans un preseed.

## 2 - Mise au domaine.

On configure les fichiers qui vont bien .

`/etc/krbrs5.conf`

```bash
[libdefaults]
	default_realm = MON.DOMAINE.ORG

# The following krb5.conf variables are only for MIT Kerberos.
	kdc_timesync = 1
	ccache_type = 4
	forwardable = true
	proxiable = true

# The following encryption type specification will be used by MIT Kerberos
# if uncommented.  In general, the defaults in the MIT Kerberos code are
# correct and overriding these specifications only serves to disable new
# encryption types as they are added, creating interoperability problems.
#
# The only time when you might need to uncomment these lines and change
# the enctypes is if you have local software that will break on ticket
# caches containing ticket encryption types it doesn't know about (such as
# old versions of Sun Java).

#	default_tgs_enctypes = des3-hmac-sha1
#	default_tkt_enctypes = des3-hmac-sha1
#	permitted_enctypes = des3-hmac-sha1

# The following libdefaults parameters are only for Heimdal Kerberos.
	fcc-mit-ticketflags = true

[realms]
	MON.DOMAINE.ORG = {
		kdc = DC.MON.DOMAINE.ORG
	}
```

`/etc/samba/smb.conf` section \[global\].

```bash
[global]

## Browsing/Identification ###

# Change this to the workgroup/NT-domain name your Samba server will part of
   workgroup = MON-GROUPEDETRAVAIL
   client signing = yes
   client use spnego = yes
   kerberos method = secrets and keytab
   realm = MON.DOMAINE.ORG
   security = ads
```

`/etc/sssd/sssd.conf`

```bash
[sssd]
services = nss, pam
config_file_version = 2
domains = MON.DOMAINE.ORG

[domain/MON.DOMAINE.ORG]
id_provider = ad
access_provider = ad

# Use this if users are being logged in at /.
# This example specifies /home/DOMAIN-FQDN/user as $HOME.  Use with pam_mkhomedir.so
override_homedir = /home/%d/%u

# Uncomment if the client machine hostname doesn't match the computer object on the DC.
# ad_hostname = mymachine.myubuntu.example.com

# Uncomment if DNS SRV resolution is not working
# ad_server = dc.mydomain.example.com

# Uncomment if the AD domain is named differently than the Samba domain
# ad_domain = MYUBUNTU.EXAMPLE.COM

# Enumeration is discouraged for performance reasons.
# enumerate = true
```

On applique les permissions qui vont bien.

```bash
# chown root:root /etc/sssd/sssd.conf

# chmod 600 /etc/sssd/sssd.conf
```

`/etc/nsswitch.conf`

```bash
# /etc/nsswitch.conf
#
# Example configuration of GNU Name Service Switch functionality.
# If you have the `glibc-doc-reference' and `info' packages installed, try:
# `info libc "Name Service Switch"' for information about this file.

passwd:         compat sss systemd
group:          compat sss systemd
shadow:         compat sss
gshadow:        files

hosts:          files mdns4_minimal [NOTFOUND=return] dns myhostname
networks:       files

protocols:      db files
services:       db files sss
ethers:         db files
rpc:            db files

netgroup:       nis sss
sudoers:        files sss
```

En ce qui concerne les DNS Ubuntu vient avec `systemd-resolved.service`

Le fichier /etc/resolv.conf qui contient les entrées DNS s'update via `systemd-resolved.service`

```bash
# systemctl stop systemd-resolved.service
```

Je le désactive pour la mise au domaine et édite `/etc/resolv.conf` à la main en lui indiquant le serveur DNS qui va bien.

```bash
nameserver x.X.x.X 
options edns0
```

`/etc/hosts`

```bash
127.0.0.1		localhost
127.0.1.1       UBUNTU1804.MON.DOMAINE.ORG UBUNTUVM110
# L'IP DE MA MACHINE
x.X.x.X  UBUNTU1804.MON.DOMAINE.ORG UBUNTU1804
```


On redémarre les services qui vont servir :

```bash
# systemctl restart chrony.service
# systemctl restart smbd.service nmbd.service 
# systemctl start sssd.service
```

On peut attaquer l'authentification du contrôleur de domaine afin de générer les tickets qui vont bien.

```bash
# kinit Administrator
```

On entre le password du contrôleur de domaine et on vérifie la présence des tickets.

```bash
# klist
Ticket cache: FILE:/tmp/krb5cc_0
Default principal: Administrator@MON.DOMAINE.ORG

Valid starting       Expires              Service principal
renew until 28/03/2020 14:31:05
27/03/2020 14:31:20  28/03/2020 00:31:12  cifs/srv.mon.domaine.org@MON.DOMAINE.ORG
27/03/2020 14:31:20  28/03/2020 00:31:12  ldap/srv.mon.domaine.org@MON.DOMAINE.ORG
```

Trop bien nos tickets sont là !

```bash
# net ads join -k
Using short domain name -- MON-GROUPEDETRAVAIL
Joined 'UBUNTU1804' to dns domain 'mon.domaine.org'
```

On vérifie tout de même avec realmd :

```bash
# realm join MON.DOMAINE.ORG
realm: Already joined to this domain
```

Le login doit créer des /home/%u

Home directories with pam\_mkhomedir (optional)

`/etc/pam.d/common-session` ajouter après session _required pam\_unix.so:_ `session required pam_mkhomedir.so skel=/etc/skel/ umask=0022`

```bash
# /etc/pam.d/common-session - session-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define tasks to be performed
# at the start and end of sessions of *any* kind (both interactive and
# non-interactive).
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
session	[default=1]			pam_permit.so
# here's the fallback if no module succeeds
session	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
session	required			pam_permit.so
# The pam_umask module will set the umask according to the system default in
# /etc/login.defs and user settings, solving the problem of different
# umask settings with different shells, display managers, remote sessions etc.
# See "man pam_umask".
session optional			pam_umask.so
# and here are more per-package modules (the "Additional" block)
session	required	pam_unix.so 
session    required    pam_mkhomedir.so skel=/etc/skel/ umask=0022
session	optional			pam_sss.so 
session	optional	pam_systemd.so 
# end of pam-auth-update config
```

Bon après cela on reboot et on essaie de se loguer avec un user du domaine et comme par magie ça fonctionne ! (ou pas.)

```bash
root@UBUNTU1804:/home/MON.DOMAINE.ORG/administrator#
```

Des pistes d&#39;investigation :

```bash
# journalctl -f -b -u sssd
# journalctl -f -b -u gdm
```