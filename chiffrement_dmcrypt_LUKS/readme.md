On propose de chiffrer un disque utilisant luks2 avec algo/cipher corrects et le header detaché (avec possibilité de le reattacher dans le futur)

C'est tres simple : 


Si l'on veut être sur de pas laisser fuiter de la matedata à balle va falloir proprement wiper votre disque :

* HDD disque magnetique la technique dd fonctionne bien : 
```bash
dd if=/dev/urandom of=/dev/sdc status=progress bs=4M 
```

* SSD/NVME On evite la technique dd et on prefere le [ata secure erase](https://ata.wiki.kernel.org/index.php/ATA_Secure_Erase) via hdparm

Une fois le disque pret on peut y aller  : 


On genere le header avec du zero : 

```bash
dd if=/dev/zero of=header.img bs=16M count=1
```

On chiffre le disque avec les parametres qui nous conviennent en precisant le header : 

Pour le offset on le decalle pour garder de l'espace en vue de le reattache run jour: Wiki Ach : 

* Tip: The --offset option allows specifying the start of encrypted data on a device. By reserving a space at the beginning of device you have the option of later reattaching the LUKS header. The value is specified in 512-byte sectors, see cryptsetup(8) for more details.

```bash
cryptsetup -y -v --use-random -h sha512 -s 512 -c aes-xts-plain64 --type luks2 --offset 32768 --header header.img luksFormat /dev/sdx
```


Naturellement on fait YES en majuscules et on entre deux fois une passphrase de fou malade !

Ok 

On dechiffre le container : 

```bash
cryptsetup open --header header.img /dev/sdx encrypted
```

Naturellement nous avons besoin d'un FS sur le container :

Choisissez ce que vous voulez apres le `mkfs.`

```bash
mkfs.btrfs /dev/sdx
```


On monte notre parittion pour y faire des trucs :

```bash
mount /dev/mapper/encrypted /mnt
```


Et voila !


RESSOURCES : 

https://wiki.archlinux.org/title/Dm-crypt/Specialties#Encrypted_system_using_a_detached_LUKS_header

https://ata.wiki.kernel.org/index.php/ATA_Secure_Erase


