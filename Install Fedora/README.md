# Delocker Fedora Luks à distance via ssh et dracut-ssh

Il faut installer les modules necessaires : 

```bash
dnf install -y dracut-network`
```
```bash
cat /etc/systemd/network/20-wired.network
```

```
[Match]
Name=e*

[Network]
DHCP=ipv4
```
On adapte au besoin l'interface.

On installe le module dracut-ssh : 

2 methodes : 

A - Manuelle git
```bash
git clone https://github.com/gsauthof/dracut-sshd
cp -ri 46sshd /usr/lib/dracut/modules.d
```
B - Automatisée copr

```bash
dnf copr enable gsauthof/dracut-sshd
```

On active et conf le module network dans dracut : 
```shell
/etc/default/grub

rd.neednet=1 ip=dhcp
```

Si besoin d'une ip fixe : 


```bash
cat /etc/dracut.conf.d/90-networkd.conf
install_items+=" /etc/systemd/network/20-wired.network "
add_dracutmodules+=" systemd-networkd "
```

On regenere l'initramfs et on update grub 

```bash
dracut -f -V
grub2-mkconfig -o /etc/grub2-efi.cfg
```

On redemarre le systeme 

```bash
systemctl reboot
```

On se connecte ensuite à la machine au reboot

```bash
ssh -i .ssh/myid -l root IP
```

On tombe sur ce message 

```shell
Welcome to the early boot SSH environment. You may type
￼
￼   systemd-tty-ask-password-agent
￼
(or press "arrow up") to unlock your disks.

This shell will terminate automatically a few seconds after the
unlocking process has succeeded and when the boot proceeds. 
```



