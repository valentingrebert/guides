---     ---

En construction (perpétuelle) 

---     ---

# [Monter un serveur PXE (DHCP TFTP NFS) capable de booter des clients en BIOS/BOOT ou UEFI/SECURE BOOT.](https://gitlab.com/valentingrebert/guides/-/blob/master/PXE/PXE_FED.md#part1)

-----

Le but de ces notes est de comprendre la mise en place du serveur PXE sur une distribution type CentOS/RHEL/Fedora.


Ce serveur PXE devra être capable de : 

* Détecter et prendre en charge les clients Bios/Boot et UEFI/Secure boot.
* Booter différentes distributions GNU/Linux ( CentOS, RHEL, Fedora, Debian, Alpine, ...)
* Distribuer des kickstarts et preseed afin de parametrer/automatiser les installations.


Il exite des alternatives tres puissantes pour gerer des installations automatiques Fog,FAI,Chief,Puppet,etc... 

Ces notes ont pour but de faire comprendre les principes basiques d'un PXE "moderne" et de comprendre les mecaniques.


------

------


Distribution utilisée: **Fedora 31 Server** 


Les protocoles réseau utilisés sont : 

* DHCP (  ISC DHCP SERVER ) dhcp-server.x86_64 **dhcpd**  port *:67 68*
* TFTP ( tftp ) tftp-server.x86_64 **tftp**  port *:69*
* NFS ( nfs v4 ) nfs-utils.x86_64 **nfs-server.service** port *:2049*


Les outils système utilisés sont : 

* Syslinux (pxelinux, vesamenu, ... ) 
* Shim (shimx64.efi)
* Grub2 (grubx64.efi)
* Kernels et initrd


Nous limiterons les services afin de ne pas surcharger le serveur et de réduire la masse de configurations, de services et de règles de pare-feu. 

Nous choisissons la version 4 du protocole NFS qui se montre efficace ne nécessite pas **rpcbind** et afin de gérer plus facilement les ACL. 

Notez que dans la version 8 de Red Hat le NFS utilise TCP et non plus UDP par défaut.

>  NFS version 4 (NFSv4) works through firewalls and on the Internet, no longer requires an rpcbind service, supports Access Control Lists (ACLs), and utilizes stateful operations. 
>  NFSv4 requires the Transmission Control Protocol (TCP) running over an IP network. 
>  In Red Hat Enterprise Linux 8, NFS over UDP is no longer supported. By default, UDP is disabled in the NFS server. 
------


Ces notes s'inspirent grandement de la __documentation officielle__ mais aussi et surtout de tests dans un lab. 


-------


Enjoy.



# Mise en place d'un serveur PXE: DHCP + TFTP + NFS. BIOS/BOOT et UEFI/SECURE.


Préambule : Nous avons conscience qu'il existe des serveurs de déploiement très puissants (ex : FOG) afin d'imager/déployer des systèmes.

Par ailleurs des outils tels kickstart/ansible , Chief, Puppet, Foreman, imagebuilder, etc ... permettent d'installer des systèmes de façon plus probante, rapide et intelligente.

Ces notes adressent à des infrastructures simples et se réfèrent à l'installation de systèmes bare-metal ou de disaster system (REAR).


__TODO LIST !__

* TODO ! (Mettre en place les kickstarts et preseed)

* TODO ! (Mettre en place des distributions :  Void Linux musl PureOS BSD QubesOS fedora Kiosk) 

* TODO ! (Mettre en place l'installation assistée par VNC sur Fedora et RedHat)

* TODO ! (Mettre en place les sécurités type *noshell* =  inst=sshd)

* TODO ! Avec RHEL 8 Fedora 31 Server.

*Avant propos: 

Testé avec une distribution Fedora 32 Server sur le HOST.  (Non testé avec Fedora WORKSTATION ni une Debian certainement pas une Buntu)



## Sommaire

[1 - Installation et configuration des services DHCP TFTP ET NFS.](#part1)

* A - Installation des outils nécessaires. 

* B - Configuration du DHCP.

* C - Configuration du TFTP et NFS (v4).
    
[2 - Préparation des répertoires et fichiers nécessaires](#part2)

* A - Création des répertoires nécessaires et copie des fichiers.

* B - Vérification des fichiers et des ACL.

[3 - Élaboration des menus BIOS/BOOT et UEFI/Secure BOOT.](#part3)

* A - BIOS/BOOT via syslinux.

* B - UEFI/SECURE via grub2.

[4 - Vérification du fonctionnement des services ](#part4)


[5 - Sécurisation du système et de la distribution des services](#part5)


[6 - Tests et Troubleshooting. ](#part6)

___


---

<a name="part1">

## 1 - Installation et configuration des serveurs DHCP TFTP ET NFS.

</a>


### A - Installation des outils nécessaires.


Nous les installons avec le puissant gestionnaire `dnf`.

```bash
dnf -y install dhcp tftp-server nfs-utils syslinux
```



### B - Configuration du DHCP.


Nous commençons par la configuration du serveur DHCP. 
  
  * Remarque : 
Le DHCP est un élément primordial de notre structure. Si vous évoluez dans une infrastructure ou le DHCP est déjà en place renseignez-vous sur la notion "authoritative" d'un serveur.
Par ailleurs nous allons voir que le DHCP et le tftp sont liés par la configuration du serveur au sein de `/etc/dhcpd.conf`.
Il est aussi possible et recommandé d'optimiser l'envoi des trames en augmentant le MTU de l'interface du server et en adaptant l'option.
En effet avec un MTU de 9000 entre le serveur et le/les clients vos déploiements seront véloces.



Si nous sommes au sein d'un réseau avec nom de domaine nous éditerons le fichier `/etc/idmapd.conf`

```console
# line 5: decommentez et précisez votre nom de domaine.

# Domain = srv.world
```

Il existe une pléthore de paramètres sur isc-dhcp-server consultez le manuel pour les connaître.
Notre configuration est assez minimaliste.

* Remarque: Les fichiers `uefi/shimx64.efi` et `pxelinux.0` ne sont pas encore en place
mais nous allons les placer plus tard à la racine du serveur tftp dans `/var/lib/tftpboot/`



Ne copiez/collez pas la configuration. Adaptez selon vos besoins. 

Changez MACADDRESS par votre adresse mac ou bien commentez l'option qui permet de spécifier une IP au host.

La configuration de **dhcpd** se fait avec le fichier `/etc/dhcp/dhcpd.conf`

```console
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
# Si vous souhaitez consulter une documentation en ligne 
# https://kb.isc.org/docs/isc-dhcp-41-manual-pages-dhcpdconf#

# Referez vous à la RFC4758 pour préciser vos options d'architecture.
# Il s’agit d'options pour servir des fichiers au serveur PXE.

option arch code 93 = unsigned integer 16;
if option arch = 00:07 {
	next-server 192.168.1.100;
filename "uefi/rhel8/shim.efi";
} else {
	next-server 192.168.1.100;
filename "bios/pxelinux.0";
}

# On précise le réseau et le masque .

subnet 192.168.1.0 netmask 255.255.255.0 {

# Si le ce serveur DHCP est le serveur officiel du LAN
# Il faut activer cette option . Si ça n'est pas le cas commentez-la.

authoritative;

# On précise la plage des adresses IP que nous allons donner à nos hosts.
# On précise également la durée du bail par défaut et sa durée maximum.
# Il s'agit ici d'un lab assez réduit .

range 192.168.1.20 192.168.1.42;
default-lease-time 600;
max-lease-time 7200;


# On donne l'ordre aux clients de taper dans les jumbo frames.
# option interface-mtu 9000;
# Les clients inconnus sur ce reseau auront tel addressage .

#pool {
#option domain-name-servers bogus.example.com;
#max-lease-time 300;
#range 10.0.0.200 10.0.0.253;
#allow unknown-clients;
#}

# Les clients connus auront tel adressage


#pool {
#option domain-name-servers ns1.example.com, ns2.example.com;
#max-lease-time 28800;
#range 10.0.0.5 10.0.0.199;
#deny unknown-clients;
#} 


# Consultez le manuel afin de préciser vos options de log
# Si vous décidez de ne pas utiliser uniquement journalctl 
# ou si vous voulez exporter vos logs.
# log-facility local7;



# Activez ou désactivez cette options afin de mettre en place une mise à jour automatique des DNS.

ddns-update-style none;

# On définit des adresses fixes par adresses MAC

#host jezero {
#hardware ethernet 02:34:37:24:c0:a5;
#fixed-address 192.168.1.43;
#}


# Au lieu d'utiliser les mac à la main on se réfère au fichier /etc/hosts

# on précise le host avec l'option booting
#host S1_fedoralab {
#   hardware ethernet MACADDRESS;
#   allow booting;
#}

# On précise l’adresse des routeurs et des serveurs DNS
option domain-name-servers 192.168.1.1;
option routers 192.168.1.1;

}

```

Pour gérer notre service *dhcpd* nous utilisons comme d'habitude `systemctl start|stop|status|enable|disable dhcpd`

```bash
systemctl start dhcpd
```

* Attention : L'activation d'un serveur DHCP sur un réseau disposant déjà d'un tel service n'est pas à faire. Ici nous considérons que notre serveur est Authoritative.
* Si vous disposez déjà d'une serveur DHCP configurez le en conséquence afin qu'il distribue les fichiers dans de bonnes conditions. (Reglage du subnet)
* Il existe toutefois une méthode avec dnsmasq afin de pallier à ce problème et utiliser le [ProxyDHCP]( https://wiki.fogproject.org/wiki/index.php?title=ProxyDHCP_with_dnsmasq) 


Pour plus de details : 

```bash
/usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid $DHCPDARG
```

Lorsque notre configuration est correcte nous pouvons activer au démarrage notre service et autoriser les paquets à passer le pare-feu:

Afin de verifier la bonne distribution d'une IP à un host qui boot via le reseau nous pouvons simplement journaliser le service: 

```bash
journalctl -f -b -u dhcp
```

```bash
systemctl enable --now dhcpd

firewall-cmd --add-service=dhcp --permanent 

firewall-cmd --reload
```

### C - Configuration du TFTP et NFS (v4)


* Remarque: IP FIXE et MTU : Afin d'obtenir une IP fixe et un MTU défini il existe plusieurs méthodes:
Après avoir loggué sur notre Host avec `journalctl -f -b` nous avons realisé que *dnsmasq* lié à NetworkManager empêchait de livrer le *tftp* correctement.
Ainsi nous avons stoppé le service *dnsmasq* le temps de la procédure.


### IP FIXE :

Si l'on souhaite obtenir une IP fixe [sans *NetworkManager*](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/8.1_release_notes/deprecated_functionality#deprecated-functionality_networking) il convient d'installer `network-scripts` et de modifier ce fichier : 

*interface* est à remplacer par votre interface Ethernet bien entendu. 

`/etc/sysconfig/network-scripts/ifcfg-interface`

```console
TYPE=Ethernet

PROXY_METHOD=none
BROWSER_ONLY=no

BOOTPROTO=static
IPADDR=192.168.1.100
NETMASK=255.255.255.0
GATEWAY=192.168.1.1
DEFROUTE=yes
IPV4_FAILURE_FATAL=no

# Dans cette conf l'ipv6 ne nous intéresse pas.
IPV6INIT=no
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy

NAME=ens3

# Ne modifiez pas votre UUID il est propre à cette connexion.
#UUID=f9e368c1-6b0e-3062-b56f-79b5af10407c

# Veillez bien à ce qu'il s'agisse' de la bonne interface réseau.
DEVICE=ens3

# Les Jumbo Frames c'est délicieux.
# MTU=9000

ONBOOT=yes
AUTOCONNECT_PRIORITY=-999

```

Pensez également à désactiver NetworkManager avec `systemctl disable --now NetworkManager` si c'est la méthode IP fixe que vous avez choisi.


### TFTP

Voici le fichier de conf initial sur FEDORA 32 Server `/usr/lib/systemd/system/tftp.service`

```shell
[Unit]
Description=Tftp Server
Requires=tftp.socket
Documentation=man:in.tftpd

[Service]
ExecStart=/usr/sbin/in.tftpd -v -s /var/lib/tftpboot
StandardInput=socket

[Install]
Also=tftp.socket
```

Techniquement vous n'avez pas besoin de le toucher. 
Mais nous avons rajouté l'option `-v` qui permet d'avoir plus de log. Quoi c'est bon les log faut en manger.




Nous devrons préciser au système de démarrer le service *tftp* . Par ailleurs il faudra ouvrir le pare-feu.

```bash
systemctl enable --now tftp.socket

systemctl enable --now tftp.service

firewall-cmd --add-service=tftp --permanent 

firewall-cmd --reload 
```



Afin de vérifier la validité du transfert via *tftp* : 

```bash
ss -lapunte | grep 69

tcpdump port 69 -v -i ens3

journalctl -f -b
```

`ens3` est à remplacer par votre interface bien entendu.



### NFS

La configuration de NFS se fait avec deux fichiers `/etc/nfs.conf` pour le service et `/etc/exports` pour servir les répertoires et fichiers.

`/etc/nfs.conf`

```shell
[nfsd]
debug=4
# threads=8
# host=
port=2048
# grace-time=90
# lease-time=90
udp=n
tcp=y
# vers2=n
# vers3=y
# vers4=y
# vers4.0=y
# vers4.1=y
vers4.2=y
# rdma=n
#
```

Notez qu'il s'agit de la version 4 du protocole NFS si vous rencontrez des problèmes passez en V3 en revanche vous devrez revoir votre politique de pare-feu et garder à l’esprit que la V3 est dépréciée.

`/etc/exports`

```shell
# NFS exports for PXE server

/srv/nfs/ 192.168.1.100/24(rw,no_root_squash)
```

Nous lançons et activons les services. Aussi nous ajoutons une règle au pare-feu.

```bash
systemctl enable --now nfs-server.service

firewall-cmd --add-service=nfs --permanent 

firewall-cmd --reload 

exportfs -rav
```



Nous pouvons vérifier l’état des ports pour les services de manière générale avec : 

```bash
ss -lapunte | grep -E 'dhcp|nfs|tftp'
```


<a name="part2">

## 2 -  Préparation des répertoires et fichiers nécessaires.

</a>


### A - Création des répertoires nécessaires et copie des fichiers.

Mettons en place les répertoires. Nous partons du principe que
vous disposez de l'iso de RHEL8: *rhel-8.0-x86_64-dvd.iso* .
Le répertoire utilisé par le tftp est */var/lib/tftpboot/* .
Nous allons séparer nos méthodes de boot pour plus de clarté .


* Pour le *tftp* : 


```bash
mkdir -p /var/lib/tftpboot/{f34w,f34s,rhel8,debian11,xubuntu2004,ubuntu2004D,arch}

mkdir -p /var/lib/tftpboot/bios/pxelinux.cfg

mkdir -p /var/lib/tftpboot/{f34w,f34s,rhel8,debian11,xubuntu2004,ubuntu2004D,arch}

mkdir -p /tmp/rhel8/{shim,grub2}
```

* Pour le nfs: 

```bash
mkdir -p /srv/nfs/pxe/{f34w,f34s,rhel8,debian11,xubuntu2004,ubuntu2004D,arch}

mkdir -p /srv/nfs/isos/
```

Nous plaçons les fichiers nécessaires au BIOS/BOOT (syslinux) dans le répertoire *pxelinux.cfg* :

```bash
cp /usr/share/syslinux/{pxelinux.0,vesamenu.c32,ldlinux.c32,libcom32.c32,libutil.c32} /var/lib/tftpboot/bios/
```

Nous plaçons les kernels (vmlinuz) et les initrd aux endroits adéquats. Ils pourront être utilisés pour les deux méthodes de boot.

```bash
cp /mnt/tmp/images/pxeboot/* /var/lib/tftpboot/rhel8/
```

Nous vérifions la présence des fichiers et dossiers :

```bash
tree -ugh /var/lib/tftpboot/
```

Pour prendre en charge les clients en UEFI nous avons besoin d'autres repertoires et fichiers:

#### Section RHEL 8

* Notes : Secure boot. 
Le principe du Secure boot est de vérifier le gestionnaire d'amorçage avec des clés publiques signées par une CA avant son lancement.
Nous devrons, avant que notre outil grub2 puisse être lancé (avec *grubx64.efi*) , utiliser le shim (signé) : *shimx64.efi*
présent dans l'iso *rhel-8.0-x86_64-dvd.iso* au sein du fichier */BaseOS/Packages/shim-x64-15-5.x86_64.rpm*
En effet le *grubx64.efi* n'est pas signé et le secure boot n'en voudrait pas. 

Nous devons aller chercher les fichiers nécessaires dans le .rpm. Deux méthodes s'offrent à nous aller les chercher sur le NET ou sur l'iso.

Nous préférons l'iso car les fichiers sont adéquats à la version de notre distribution et directement accessibles.

Pour cela nous montons l'iso et choisissons d'extraire les fichiers dans `/tmp/rhel8/`. 

Pour le moment nous mettons le fichier *grubx64.efi* dans *uefi/rhel8* ce fichier est primordial il permettra de lancer le gestionnaire d'amorçage grub2. 

```shell
mount -o loop rhel-8.0-x86_64-dvd.iso /mnt/tmp/

rpm2cpio /mnt/tmp/BaseOS/Packages/grub2-efi-x64-2.02-66.el8.x86_64.rpm | cpio -idv -D /tmp/rhel8/grub2/

cp -r /tmp/rhel8/grub2/boot/efi/EFI/redhat/* /var/lib/tftpboot/uefi/rhel8/
```

Le fichier *shimx64.efi* contient les signatures nécessaires au Secure Boot. 

```shell
rpm2cpio /mnt/tmp/BaseOS/Packages/shim-x64-15-5.x86_64.rpm | cpio -idv -D /tmp/rhel8/shim/

cp -r /tmp/rhel8/shim/boot/efi/EFI/redhat/* /var/lib/tftpboot/uefi/rhel8/

rm -rf /tmp/rhel8/shim
```

Pendant que nous y sommes nous plaçons les fichiers essentiels : *initrd.img* et *vmlinuz* aux endroits adéquats.


Copions également les fonts unicode de grub. 

```shell
rpm2cpio /mnt/tmp/BaseOS/Packages/grub2-efi-x64-cdboot-2.02-66.el8.x86_64.rpm | cpio -idv -D /tmp/rhel8/grub2/

cp -r /tmp/rhel8/shim/boot/efi/EFI/redhat/fonts/unicode.pf2 /var/lib/tftpboot/

rm -rf /tmp/rhel8/
```
La plupart de nos fichiers sont en place.


#### Section Ubuntu


Les parametres de boot de base niveau tftp

https://help.ubuntu.com/lts/installation-guide/amd64/ch04s05.html


Les boot options necessaires à la construction d'un menu .

https://help.ubuntu.com/lts/installation-guide/amd64/ch05s03.html

Nous apprendrons ici

https://help.ubuntu.com/lts/installation-guide/amd64/ch06s03.html#di-user-setup

concernant iso-scan  qu'il ne va pas au dela de 1 niveau de sous repertoire lors de sa recherche d'iso. Fallait le savoir.

### B - Vérification des fichiers et des ACL.


Voyons le contenu de notre dossier racine du seveur tftp. 

`/var/lib/tftpboot/`

```shell
drwxr-xr-x  8 root root 4,0K sept. 17 02:46 .
drwxr-xr-x 20 root root 4,0K sept.  5 20:31 ..
drwxr-xr-x  3 root root 4,0K sept.  9 20:06 bios
drwxr-xr-x  3 root root 4,0K sept. 10 14:10 debian-installer
drwxr-xr-x  8 root root 4,0K sept.  9 20:01 kernels
drwxr-xr-x  2 root root 4,0K sept. 10 12:49 kickstart
drwxr-xr-x  2 root root 4,0K sept. 15 23:42 preseed
drwxr-xr-x  7 root root 4,0K sept. 10 14:08 uefi
```


<a name="part3">

## 3 - Élaboration des menus BIOS/BOOT et UEFI/Secure BOOT.

</a>


Nous allons désormais préparer les menus de boot. 

Pour le menu *BIOS/BOOT* il s'agit du fichier  contenu dans `/var/lib/tftpboot/pxelinux.cfg/default`

[les recommandations de *Red Hat*](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/performing_an_advanced_rhel_installation/preparing-for-a-network-install_installing-rhel-as-an-experienced-user) sont assez claires


`/var/lib/tftpboot/pxelinux.cfg/default`


```shell
UI vesamenu.c32
prompt 1
timeout 600
MENU RESOLUTION 1024 768
MENU TITLE Bienvenue sur le PXE RHEL8
MENU BACKGROUND background.png
display boot.msg

label fedoraw31a
	menu label ^Install Fedora Workstation 31 64-bit
	kernel kernels/f31w/vmlinuz
	append initrd=kernels/f31w/initrd.img inst.stage2=nfs:192.168.100.10:/srv/nfs/pxe/fedora31w/ ip=dhcp

label fedoraw31b
	menu label ^Start Fedora-Workstation-Live 31
	kernel kernels/f31w/vmlinuz
	append initrd=kernels/f31w/initrd.img root=nfs:192.168.100.10:/srv/nfs/pxe/fedora31w/  rd.live.image quiet

label fedoras31a
	menu label ^Install Fedora 31 ( Minimal Image )
	kernel kernels/f31s/vmlinuz
	append initrd=kernels/f31s/initrd.img inst.stage2=nfs:192.168.1.1:/srv/nfs/pxe/fedora31s/ ip=dhcp ks=nfs:192.168.100.10:/srv/nfs/pxe/fedora31/kickstarts/minimal.ks

label fedoras31b
    menu label ^Install Fedora 31 Special Kisckstart
	kernel kernels/f31s/vmlinuz
	append initrd=kernels/f31s/initrd.img inst.stage2=nfs:192.168.100.10:/srv/nfs/pxe/fedora31s/ ip=dhcp ks=nfs:192.168.100.10:/srv/nfs/pxe/fedora31s/kickstarts/special.ks
	#inst.repo=nfs:[<options>:]<server>:/<path>

label rhel8a
	menu label ^Install RHEL8 Assisté via VNC
	kernel kernels/rhel8/vmlinuz
	append initrd=kernels/rhel8/initrd.img inst.stage2=nfs:192.168.100.10:/srv/nfs/pxe/rhel8/ ip=dhcp
	
label rhel8normal
    menu label ^Install RHEL8 via ISO
    kernel kernels/rhel8/vmlinuz
    append initrd=kernels/rhel8/initrd.img ip=dhcp inst.repo=nfs:nfsvers=4:192.168.100.10:/srv/nfs/isos/rhel-8.0-x86_64-dvd.iso 

label rhel8vesa
    menu label ^Install RHEL8 avec basic video driver
    kernel kernels/rhel8/vmlinuz
    append initrd=kernels/rhel8/initrd.img ip=dhcp inst.xdriver=vesa nomodeset inst.repo=nfs:nfsvers=4:192.168.100.10:/srv/nfs/isos/rhel-8.0-x86_64-dvd.iso 

label rhel8rescue
    menu label ^Reparer RHEL8 Rescue
    kernel kernels/rhel8/vmlinuz
    append initrd=kernels/rhel8/initrd.img rescue

label PureOS
	menu label ^Install PureOS gnome 64
	kernel kernels/pureos/vmlinuz
	append initrd=kernels/pureOS/initrd.img inst.stage2=nfs:192.168.100.10:/srv/nfs/pureos/ ip=dhcp

label local
	menu label Boot from ^local drive
	localboot 0xffff
```

Pour les clients *UEFI/SB* nous plaçons notre menu dans le répertoire *uefi* il se nommera *grub.cfg* afin que lorsque *grubx64.efi*
se lance il détecte automatiquement la configuration.

`/var/lib/tftpboot/uefi/rhel8/grub.cfg`

[/var/lib/tftpboot/uefi/rhel8/grub.cfg](https://gitlab.com/valentingrebert/guides/-/blob/master/pxe_preseed/grub.cfg))


### ubuntu Stuff

Afin de lancer le grub d'Ubuntu signé il faudra se procurer les fichiers suivants: 

(http://archive.ubuntu.com/ubuntu/dists/bionic-updates/main/uefi/grub2-amd64/current/grubx64.efi.signed)
(http://archive.ubuntu.com/ubuntu/dists/bionic-updates/main/uefi/grub2-amd64/current/grubnetx64.efi.signed)

* shim.efi.signed 

(https://packages.ubuntu.com/bionic-updates/shim-signed)



<a name="part4">

## 4 - Vérification du fonctionnement des services ]

</a>





## Troubleshooting : 

Si vous rencontrez des problèmes relatifs à la mise en place du service *tftp* .


* tftp troubleshooting.

Ajoutez une option de log verbeux afin de mieux journaliser avec *journalctl*.


`/etc/systemd/system/tftp-server.service`

```shell
...
ExecStart=/usr/sbin/in.tftpd -c -s -v -p /var/lib/tftpboot
...
```

On redemarre le service :

```shell
systemctl daemon-reload
systemctl restart tftp-server.service
```

On ajoute l'option `-v` pour verbose.

Ainsi lorsqu'on loggue avec `journatlctl -f -b -u tftp-server.service`

```
mars 04 13:42:34 fedora31.locallab in.tftpd[2029]: tftp: client does not accept options
mars 04 13:42:34 fedora31.locallab in.tftpd[2030]: RRQ from ::ffff:192.168.100.89 filename uefi/xubuntu1804/grubnetx64.efi.signed
mars 04 13:42:34 fedora31.locallab in.tftpd[2030]: Client ::ffff:192.168.100.89 finished uefi/xubuntu1804/grubnetx64.efi.signed
mars 04 13:42:34 fedora31.locallab in.tftpd[2031]: RRQ from ::ffff:192.168.100.89 filename /grub/x86_64-efi/command.lst
mars 04 13:42:34 fedora31.locallab in.tftpd[2031]: Client ::ffff:192.168.100.89 File not found /grub/x86_64-efi/command.lst
mars 04 13:42:34 fedora31.locallab in.tftpd[2032]: RRQ from ::ffff:192.168.100.89 filename /grub/x86_64-efi/fs.lst
mars 04 13:42:34 fedora31.locallab in.tftpd[2032]: Client ::ffff:192.168.100.89 File not found /grub/x86_64-efi/fs.lst
mars 04 13:42:34 fedora31.locallab in.tftpd[2033]: RRQ from ::ffff:192.168.100.89 filename /grub/x86_64-efi/crypto.lst
mars 04 13:42:34 fedora31.locallab in.tftpd[2033]: Client ::ffff:192.168.100.89 File not found /grub/x86_64-efi/crypto.lst
mars 04 13:42:34 fedora31.locallab in.tftpd[2034]: RRQ from ::ffff:192.168.100.89 filename /grub/x86_64-efi/terminal.lst
mars 04 13:42:34 fedora31.locallab in.tftpd[2034]: Client ::ffff:192.168.100.89 File not found /grub/x86_64-efi/terminal.lst
mars 04 13:42:34 fedora31.locallab in.tftpd[2035]: RRQ from ::ffff:192.168.100.89 filename /grub/grub.cfg
mars 04 13:42:34 fedora31.locallab in.tftpd[2035]: Client ::ffff:192.168.100.89 File not found /grub/grub.cfg
```


### Resources:


__Base DHCP + TFTP :__

https://docs.fedoraproject.org/en-US/fedora/f34/install-guide/advanced/Network_based_Installations/


__NFS :__

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/index

https://doc.fedora-fr.org/wiki/Partage_de_disques_en_r%C3%A9seau_avec_NFS

__Preseed :__

https://serverfault.com/questions/1077958/preseeding-debian-11-fully-unattended-get-rid-of-questions/1077960?noredirect=1#comment1406907_1077960



__PXE : __

https://docs.fedoraproject.org/en-US/fedora/f34/install-guide/advanced/Network_based_Installations/

https://doc.fedora-fr.org/wiki/Partage_de_disques_en_r%C3%A9seau_avec_NFS

__GRUB : __

https://www.gnu.org/software/grub/manual/grub/html_node/Network.html#Network


__Secure Boot : __

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system_administrators_guide/sec-uefi_secure_boot


https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/assembly_securing-rhel-during-installation-security-hardening

__Ubuntu Stuff :__

https://wiki.ubuntu.com/UEFI/SecureBoot/PXE-IPv6

https://wiki.ubuntu.com/UEFI/PXE-netboot-install

https://wiki.debian-fr.xyz/PXE_avec_support_EFI

------

