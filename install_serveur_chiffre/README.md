# Installation d'un serveur chiffré unlockable à distance EFI LUKS LVM BTRFS MANUELLE ET PRESEED/KICKSTART. Debian 11 et Fedora 35.


## Installation manuelle Debian 11 Bullseye.

Nous installons Debian 11 avec l'installeur expert.

Nous partitionnons de la sorte :

Nous créeons sur le disque `/dev/sda`

a. La premiere partition EFI  `/boot/efi` 512MiB

b. Une seconde partition de boot (on prevoit plus que necessaire au cas ou) `/boot` 1GiB

c. Une patition chiffrée *luks2* qui va accueillir le LVM 237GiB

d. Un VG qui prend la place necessaire avec deux LV un root et un swap 7.4GiB

e. Nous formatons le lv root en btrfs.

f. Nous mkswapons la swap

```
sda                       8:0    0 238.5G  0 disk
├─sda1                    8:1    0   487M  0 part  /boot/efi
├─sda2                    8:2    0   954M  0 part  /boot
└─sda3                    8:3    0 237.1G  0 part
  └─sda3_crypt          254:0    0 237.1G  0 crypt
    ├─nuxluks-pve--root 254:1    0   183G  0 lvm   /
    └─nuxluks-pve--swap 254:2    0   7.4G  0 lvm   [SWAP]
```

Il faut proceder à un partitionnement manuel. 

* Si nous avions utilisé le guided lvm encrypted il aurait tout mis en ext4 et c'est relou.

Techniquement l'installation se passe bien.

* ATTENTION: On reboot car le serveur est physiquement accessible mais : 
(S'il s'agit d'un serveur distant et que nous rebootons nous ne pourrons plus acceder au serveur sans IPMI car luks demandera la mot de passe.)


### 2 - Configuration de LUKS/dropbear-ssh pour deverouillage distant

Nous allons configurer notre initramfs pour qu'il permette de deverouiller le systeme à distance.
Il faut l'installer ajouter la clé ed25519 .pub 

```bash
apt install dropbear-initramfs cryptsetup-initramfs
echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJEe3gF//znGIzq30frI6O9qDn5eM6uqEZZlx7mR5SiS' > /etc/dropbear-initramfs/authorized_keys
```

On modifie cette ligne de la sorte dans le fichier :

`/etc/dropbear-initramfs/config`


`DROPBEAR_OPTIONS="-j -k -p 22 -s -c /bin/cryptroot-unlock "`

* -j -k pour empecher le port forwarding local et distant.
* -p 22 pour specifier le port.
* -s pour empecher l'authentification par password.
* -c /bin/cryptroot-unlock afin d'empecher toute utilisateur connecté de faire autre chose que cryptroot-unlock.


On modifie cette ligne dans le fichier 

`/etc/initramfs-tools/initramfs.conf`

Pout une ip fixe

`IP=192.168.1.30::192.168.1.1:255.255.255.0:localhost`

Ou alors pour une ip dhcp

`IP="dhcp"`


Ok tout semble bon on va mettre à jour l'initramfs avec toutes ces directives : 

```bash
update-initramfs -u
```


## Installation manuelle Fedora


L'installation par GUI est tres simple. Il suffit d'utiliser blivet pour une grande flexibilité. 
Le même partitionnement que Debian est tout à fait possible.

```shell
sda                       8:0    0 238.5G  0 disk
├─sda1                    8:1    0   487M  0 part  /boot/efi
├─sda2                    8:2    0   954M  0 part  /boot
└─sda3                    8:3    0 237.1G  0 part
  └─sda3_crypt          254:0    0 237.1G  0 crypt
    ├─nuxluks-fed 254:1    0   183G  0 lvm   /
    └─nuxluks-swap 254:2    0   7.4G  0 lvm   [SWAP]
```


Il faut installer les modules necessaires : 

```bash
dnf install -y dracut-network`
```
```bash
cat /etc/systemd/network/20-wired.network
```

```
[Match]
Name=e*

[Network]
DHCP=ipv4
```
On adapte au besoin l'interface.

On installe le module dracut-ssh : 

2 methodes : 

A - Manuelle git
```bash
git clone https://github.com/gsauthof/dracut-sshd
cp -ri 46sshd /usr/lib/dracut/modules.d
```
B - Automatisée copr

```bash
dnf copr enable gsauthof/dracut-sshd
```

On active et conf le module network dans dracut : 
```shell
/etc/default/grub

rd.neednet=1 ip=dhcp
```

On prepare dracut

`/etc/dracut.conf.d/90-networkd.conf`

```shell
install_items+=" /etc/systemd/network/20-wired.network "
add_dracutmodules+=" systemd-networkd "
```

On regenere l'initramfs et on update grub 

```bash
dracut -f -V
grub2-mkconfig -o /etc/grub2-efi.cfg
```

On redemarre le systeme 

```bash
systemctl reboot
```

On se connecte ensuite à la machine au reboot

```bash
ssh -i .ssh/myid -l root IP
```

On tombe sur ce message 

```shell
Welcome to the early boot SSH environment. You may type
￼
￼   systemd-tty-ask-password-agent
￼
(or press "arrow up") to unlock your disks.

This shell will terminate automatically a few seconds after the
unlocking process has succeeded and when the boot proceeds. 
```

Enjoy ! 


# Installation automatique Debian 11.

Preseed.

Il faut d'abord [preparer un serveur PXE.](https://gitlab.com/valentingrebert/guides/-/tree/master/pxe_preseed)

Il faudra y uploader [ce preseed](https://gitlab.com/valentingrebert/guides/-/blob/master/pxe_preseed/debseed11_crypt.cfg)

Et c'est un bingo !

On peut ensuite y appliquer des playbooks/roles ansible.

### RESSOURCES : 

https://wiki.debian.org/chroot

https://www.dwarmstrong.org/remote-unlock-dropbear/

https://www.arminpech.de/2019/12/23/debian-unlock-luks-root-partition-remotely-by-ssh-using-dropbear/

https://www.mybluelinux.com/how-to-enable-full-disk-encryption-with-encrypted-boot-root-partition-and-ramdisk-in-debian-ubuntu-linux/

https://www.dwarmstrong.org/fde-debian/


https://gist.github.com/roybotnik/b0ec2eda2bc625e19eaf
