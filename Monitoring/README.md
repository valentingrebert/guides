# Choix et installation d'un serveur de Monitoring.

### Préambule :


La mise en place du monitoring est primordiale au sein d'une infrastructure. 
Il faut distinguer cependant , serveurs de logs, serveurs supervision, monitoring et métrologie. 

Il existe une pléthore de solutions libres afin de mettre en place un monitoring efficace. 

(en vrac : netdata , collectd, prometheus, zabbix, shinken, grafana, etc ...) TODO Tableau comparatif.

Nous devons au préalable analyser nos besoins!

Il serait par exemple totalement demesuré d'installer une solution de supervision de type Nagios pour superviser (ne pas confondre avec monitorer) un seul serveur Web. 

En revanche si l'on veut mutualiser les logs et le monitoring sur plusieurs grappes de serveurs des solutions telles que : [collectd](https://collectd.org/)/[grafana](https://grafana.com/)/[prometheus](https://prometheus.io/)/ , [ELK](https://www.elastic.co/what-is/elk-stack) sont tout à fait adaptées.

Par ailleurs une solution comme [__prometheus__](https://prometheus.io/) est parfaite pour monitorer des VE (Virtual Environnement) de type __Docker,Kubernetes,Openshift.__ 

Nous allons dans un premier temps aborder les differents concepts qui compose le monde du monitoring.


1 - Kdump
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_basic_system_settings/getting-started-with-system-administration_configuring-basic-system-settings#basics-kdump

2 - Coredumpctl

3- Journalctl